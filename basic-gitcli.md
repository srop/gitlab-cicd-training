# Git and GitLab Workshop
Please download https://git-scm.com/downloads and install.

## Prepare Git Repository

* Config git user and email for commit user data first

## Contents
- 1.1 Git config global
- 1.2 Git commit 
- 1.3 Push to remote repository 
- 1.4 Create your subgroup 
- 1.5 Create first project 
- 1.6 Update your friend's source code
- 1.7 Create branch 
- 1.8 Create merge request 

## 1.1 Open Gitbash

```bash
git config --global user.name "your-name"
git config --global user.email "your-name@your-domain-company"

# Example config 
git config --global user.name "sattaya banbua"
git config --global user.email "sattaya@got.co.th"

# See your git config
git config --list
```



## 1.2 First Git Commit

* Put command `git status` to see repository status
* Put commands below for first commit

```bash
git add 'new files'
git status
git commit -m "Initial commit"
git status
```

* Put command `git log` or `git log --oneline` to see history of commit 

</br>

## 1.3  Push Repository to GitLab
1. สร้าง SSH key pair จากในเครื่อง client โดยเลือกประเภทจากด้านล่าง
    - 1.1 ED25519 

    - 1.2 RSA

2. เลือกใช้ command ตามด้านล่าง และใส่ข้อมูลให้เรียบร้อย
  #### For example, for ED25519:

``` 
ssh-keygen -t ed25519 -C "<comment>" 
```

  #### For 2048-bit RSA:

```
ssh-keygen -t rsa -b 2048 -C "<comment>"
```

```bash 
cat ~/.ssh/id_rsa.pub
# Copy your public key  
```
### Add your SSH Public Key to GitLab

* Go to <https://gitlab.com> and login with your credential
* Go to <https://gitlab.com/-/profile/keys> or menu `Settings` on your avatar icon on the top right and choose menu `SSH Keys` on the left
* Put your public key on the `Key` textbox and click `Add key`

## 1.4 Create your own group and subgroup

* Go to `Groups` > `Your group` menu on the top left
* Click on `Your group` group
* Click on `New subgroup` on the top right then fill your information and ckick `Create subgroup`

### Manage member permission 
- At the group or project level
- Go to manage menu > member
- Choose a role for your member

## 1.5 Create your first project

* Click on your newly created subgroup `[your-name]`
* Click on New project
* Create nginx project
  * Project name: nginx

### Add remote repository and push code

* Copy `git remote add origin` command in `Push an existing folder` section
* Push code to GitLab On Cloud Shell

```bash
git remote add origin git@gitlab.com:[your-group]/[your-name]/nginx.git

# To see remote repository has been added
git remote -v
git push -u origin master
# Maybe you need to answer yes for the first time push
```

* Refresh nginx main page on GitLab again to see change

## Adding nginx source code to repository

* On linux shell, `mkdir src` to create src directory
* Copy [index.html](../src/index.html)  to your `src` directory
* Copy [hello.conf](../src/hello.conf) to your `src` directory
* Commit and push the code

```bash
git status
git add .
git status
git commit -m "Adding index.html"
git push
```

## 1.6 Let's update your friend's source code

* Ask for repository url from people next to you. It is on the menu `Clone` > `Clone with SSH` in repository main page
* Clone your friend's source code on Cloud Shell

```bash
# To make sure you are on home directory again
cd
pwd

# Clone repository into [your-friend]-nginx directory
git clone git@gitlab.com:[your-group]/[your-friend]/nginx.git [your-friend]-nginx
cd [your-friend]-nginx
```

* add another word to file `README.md` of your friend's repository

```markdown
test word
```

* Commit and push your change

```bash
git status
git add README.md
git status
git commit -m "Add License"
git push
```

* Change directory back to your code and update your code

```bash
cd ../nginx
git pull
```

## 1.7 Create dev branch for develop

* Put these commands to create dev branch

```bash
git branch
git branch dev
git branch
git checkout dev
git branch
git push origin dev
```

## .gitignore file

* Create new file name `.gitignore` and push these content

```gitignore
.project
.settings
```

## 1.8 Merge Requests

* Go to menu `Merge Requests` on GitLab
* Click on `Create merge request`
* See `Commits` and `Changes` tabs. You can leave everything default and click on `Submit merge request`
* Since you are maintainer, you can click on `Merge` button to merge code from `dev` to `master` branch.
* See your changes on master branch


Finally files directory will be shown below structure, then please commit and push to your remote repo.

```
.
├── .gitignore
└── src
    ├── hello.conf
    └── index.html
```
